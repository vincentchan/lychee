layui.config({
	base : "../static/js/"
}).use(['form','layer'],function(){
	var form = layui.form(),
		layer = parent.layer === undefined ? layui.layer : parent.layer,
		$ = layui.jquery;
	//video背景
	$(window).resize(function(){
		if($(".video-player").width() > $(window).width()){
			$(".video-player").css({"height":$(window).height(),"width":"auto","left":-($(".video-player").width()-$(window).width())/2});
		}else{
			$(".video-player").css({"width":$(window).width(),"height":"auto","left":-($(".video-player").width()-$(window).width())/2});
		}
	}).resize();
	
	//登录按钮事件
	form.on("submit(login)",function(data){
		//alert(data);
		//window.location.href = "../../index.html";
        var imageCode = $('[name="code"]').val();
        if(imageCode.length != 5){
            layer.msg('验证码输入错误！');
            return false;
        }
        var pwd = $('[name="password"]').val();
        $('[name="password"]').val(hex_md5(pwd));

        //this.action = "${BASE_PATH }admin/login";
        //this.submit();
		return true;
	})
})
