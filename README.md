# lychee

#### 项目介绍
本模板基于Layui 1.0.9实现，lychee 取 Layui 中的 ly，恰好荔枝（lychee）含ly开头，那就叫lychee后台管理系统模板咯，lychee后端使用jboot+beetl+mysql，前端layui，开发后端管理系统使用这套模板，分分钟完成系统框架搭建。你只需关注具体业务开发，不需要额外的开发系统基本功能，只为开发更快，lychee致力于敏捷开发。
一图胜千言，图片走一波。
![输入图片说明](https://gitee.com/uploads/images/2018/0511/173706_376d69c8_22738.png "1.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0511/173716_cde477d7_22738.png "2.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0511/173727_9abfea3b_22738.png "3.png")


#### 软件架构
后端基于jboot开发，使用beetl模板，数据库mysql，前端Layui ，集成了dubbo，spring可以满足分布式服务的对接。


#### 使用说明

1. 下载源码或者直接idea拉取代码
2. 开发工具推荐idea，本工程使用 maven构建，如果使用其他开发工具，请自行配置maven环境
3. jdk版本不得低于1.7
4. 用户admin 密码 ts123456

#### 版本说明

1. xxx
2. xxx
3. xxx
4. xxx


